
### Assumptions
- I assume company names are unique.
- A single resource (for the REST API) is considered a company and all rdf triplets with that company as subject. 

### Execution instructions
- Create a test knowledge base, if you don't have one already, using the create_test_KB.py script
- Set the environmental variable *ONTOLOGY_SOURCE* to point to the path of the ontology: `export ONTOLOGY_SOURCE='my/ontology/path.turtle`
- Execute `python ontology_endpoint/app.py`  

### Implementation choices:
- I chose to create the class env that holds environmental variables because I find it easier to dockerise and execute later on.
- I abstracted away the KB connection via CompaniesOntology. Makes back-end changes easier in the future.

### Next steps:
I would do the following things if that was a real application: 
- Swagger definitions
- Request schema validation
- Write unit tests
- Better error handling
- Write logs
- Dockerize
- Use celery in order to server more than one client at a time (needs thread safety)
- Make calls asynchronous, unless it was guaranteed that they take such a short amount of time (e.g. tiny KB)
- If KB was getting large, I would find another framework that does not need to load the whole KB in memory. 
There would probably be some performance issues to consider as well.


### Companies ontology
Example ontology in turtle format: 

```
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix my_scape: <http://www.example.com/business#> .
@prefix org: <http://www.w3.org/ns/org#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

my_scape:FinTechCompany rdfs:subClassOf org:FormalOrganization .
my_scape:MachineLearningCompany rdfs:subClassOf org:FormalOrganization .
my_scape:HealthCareCompany rdfs:subClassOf org:FormalOrganization .

my_scape:LargeCompany rdfs:subClassOf org:FormalOrganization .
my_scape:SmallCompany rdfs:subClassOf org:FormalOrganization .

my_scape:OpenAI a my_scape:MachineLearningCompany,
        my_scape:SmallCompany .

```


