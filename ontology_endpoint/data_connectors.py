from rdflib import Graph, Literal, BNode, Namespace, RDF, URIRef, RDFS
from rdflib.namespace import DC, FOAF
import json


class CompaniesOntology:
    _graph = None
    _ontology_ref = None

    _MY_SPACE = Namespace('http://www.example.com/business#')
    _ORG = Namespace('http://www.w3.org/ns/org#')

    def __init__(self, ontology_ref):
        self._graph = Graph()
        self._ontology_ref = ontology_ref
        self._load()

    def _load(self):
        self._graph.parse(self._ontology_ref, format='turtle')
        self._bind_namespaces()

    def _bind_namespaces(self):
        if self._graph is not None:
            self._graph.bind('my_scape', self._MY_SPACE)
            self._graph.bind('org', self._ORG)
            self._graph.bind("dc", DC)
            self._graph.bind("foaf", FOAF)

    def _save(self):
        with open(self._ontology_ref, 'bw') as out_file:
            self._graph.serialize(format='turtle', destination=out_file)

    def _clear_graph(self):
        self._graph = Graph()

    def get_company(self, company_id):
        query_text = "SELECT ?p ?o WHERE { my_scape:" + str(company_id) + " ?p ?o . }"
        pred_obj_pairs = self._graph.query(query_text)
        result_graph = Graph()
        subject = str(self._MY_SPACE) + str(company_id)
        for row in pred_obj_pairs:
            result_graph.add((URIRef(subject), row[0], row[1]))
        return json.loads(result_graph.serialize(format='json-ld').decode())

    def add_company(self, data):
        # Additional input constraints should be implemented here
        self._graph.parse(format='json-ld', data=json.dumps(data))
        self._save()

    def remove_company(self, company_id):
        subject = self._MY_SPACE[company_id]
        pred_obj_pairs = list(self._graph.predicate_objects(subject=subject))
        if len(pred_obj_pairs) == 0:
            raise LookupError
        for pair in pred_obj_pairs:
            self._graph.remove((subject, pair[0], pair[1]))
        self._save()

    def get_all_triplets(self, return_format='json-ld'):
        return json.loads(self._graph.serialize(format=return_format).decode())
