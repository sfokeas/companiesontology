from flask import Flask
from ontology_endpoint.data_connectors import CompaniesOntology
from flask import jsonify, request, Response
import json
from ontology_endpoint.env import env

app = Flask(__name__)
ontology = CompaniesOntology(env.ONTOLOGY_SOURCE)


@app.route('/')
def index():
    return 'Server Works!'


@app.route('/companies/<company_name>', strict_slashes=False)
def get_individual(company_name):
    company_triplets = ontology.get_company(company_name)
    return jsonify(company_triplets)


@app.route('/companies', methods=['POST'], strict_slashes=False)
def add_individual():
    data = request.json
    data_subject = extract_subject_from_json_ld(data)
    ontology.add_company(data=data)
    response_json = json.dumps({
        'success': True,
        'location': env.SERVICE_URL + "/companies/" + data_subject
    })
    return Response(response_json, status=201, mimetype='application/json')


def extract_subject_from_json_ld(rdf_data):
    rdf_id = rdf_data['@id']
    return rdf_id[rdf_id.find('#') + 1:]


@app.route('/companies/<company_name>', methods=['DELETE'], strict_slashes=False)
def remove_individual(company_name):
    try:
        ontology.remove_company(company_name)
        return Response(status=200)
    except LookupError:
        return Response(status=404)


@app.route('/companies', methods=['GET'], strict_slashes=False)
def get_knowledge_base():
    count = request.args.get('count', default=1000, type=int)
    offset = request.args.get('offset', default=0, type=int)
    ontology_triplets = ontology.get_all_triplets(return_format='json-ld')
    return jsonify(ontology_triplets[offset:offset + count])


if __name__ == '__main__':
    app.run(host=env.APP_HOST, port=env.APP_PORT)
