import os


class Env:
    def __init__(self):
        self.ONTOLOGY_SOURCE = os.environ.get('ONTOLOGY_SOURCE', default='example_ontology.turtle')
        self.APP_HOST = os.environ.get('APP_HOST', default='127.0.0.1')
        self.APP_PORT = os.environ.get('APP_PORT', default='5000')
        self.SERVICE_URL = self.APP_HOST + ':' + self.APP_PORT
env = Env()
