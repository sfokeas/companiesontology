from rdflib import Graph, Literal, BNode, Namespace, RDF, URIRef, RDFS
from rdflib.namespace import DC, FOAF

g = Graph()

MY_SPACE = Namespace('http://www.example.com/business#')
ORG = Namespace('http://www.w3.org/ns/org#')
g.bind('my_scape', MY_SPACE)
g.bind('org', ORG)
g.bind("dc", DC)
g.bind("foaf", FOAF)

# Class definition
g.add((MY_SPACE.SmallCompany, RDFS.subClassOf, ORG.FormalOrganization))
g.add((MY_SPACE.LargeCompany, RDFS.subClassOf, ORG.FormalOrganization))
g.add((MY_SPACE.MachineLearningCompany, RDFS.subClassOf, ORG.FormalOrganization))
g.add((MY_SPACE.FinTechCompany, RDFS.subClassOf, ORG.FormalOrganization))
g.add((MY_SPACE.HealthCareCompany, RDFS.subClassOf, ORG.FormalOrganization))

# Add individuals
g.add((MY_SPACE.OpenAI, RDF.type, MY_SPACE.SmallCompany))
g.add((MY_SPACE.OpenAI, RDF.type, MY_SPACE.MachineLearningCompany))

g.add((MY_SPACE.Bayer, RDF.type, MY_SPACE.LargeCompany))
g.add((MY_SPACE.Bayer, RDF.type, MY_SPACE.HealthCareCompany))

g.add((MY_SPACE.Nurx, RDF.type, MY_SPACE.HealthCareCompany))
g.add((MY_SPACE.Nurx, RDF.type, MY_SPACE.SmallCompany))

g.add((MY_SPACE.Novartis, RDF.type, MY_SPACE.LargeCompany))
g.add((MY_SPACE.Novartis, RDF.type, MY_SPACE.HealthCareCompany))

g.add((MY_SPACE.Babylon_health, RDF.type, MY_SPACE.LargeCompany))
g.add((MY_SPACE.Babylon_health, RDF.type, MY_SPACE.HealthCareCompany))
g.add((MY_SPACE.Babylon_health, RDF.type, MY_SPACE.MachineLearningCompany))

g.add((MY_SPACE.DeepCode, RDF.type, MY_SPACE.SmallCompany))
g.add((MY_SPACE.DeepCode, RDF.type, MY_SPACE.MachineLearningCompany))

g.add((MY_SPACE.Forecast, RDF.type, MY_SPACE.SmallCompany))
g.add((MY_SPACE.Forecast, RDF.type, MY_SPACE.MachineLearningCompany))

g.add((MY_SPACE.Darktrace, RDF.type, MY_SPACE.LargeCompany))
g.add((MY_SPACE.Darktrace, RDF.type, MY_SPACE.MachineLearningCompany))

g.add((MY_SPACE.Finanzguru, RDF.type, MY_SPACE.SmallCompany))
g.add((MY_SPACE.Finanzguru, RDF.type, MY_SPACE.FinTechCompany))
g.add((MY_SPACE.Finanzguru, RDF.type, MY_SPACE.MachineLearningCompany))

g.add((MY_SPACE.Movedigital, RDF.type, MY_SPACE.SmallCompany))
g.add((MY_SPACE.Movedigital, RDF.type, MY_SPACE.FinTechCompany))
g.add((MY_SPACE.Movedigital, RDF.type, MY_SPACE.MachineLearningCompany))

print(g.serialize(format='turtle').decode())
with open('example_ontology.turtle', 'bw') as out_file:
    g.serialize(format='turtle', destination=out_file)
